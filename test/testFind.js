const find = require("../find");

console.log(find([1, 2, 3, 4], (e) => e == 2));
console.log([1, 2, 3, 4].find((e) => e == 2));

console.log(find([1, 2, 3, 4, 5], (e) => e > 0));
console.log([1, 2, 3, 4, 5].find((e) => e > 0));
console.log(
  [1].find((e) => {
    e == 0;
  })
);
console.log(find());
console.log(find([], () => {}));
console.log(find(1, (e) => e > 0));
