const filter = require("../filter");

let arr = [1, 2, 3, 4, 1, 2, 3, 4, 6, 7, 8, 9, 32, 34, 23535, 52, 21, 12, 3];
console.log(
  filter(arr, (e, i) => {
    if (i <= 5) return e <= 3;
  })
);
console.log(
  arr.filter((e, i) => {
    if (i <= 5) return e <= 3;
  })
);
console.log(filter(arr, (e) => e < 10));
console.log(arr.filter((e) => e < 10));
console.log(filter());
console.log(filter([], () => {}));
