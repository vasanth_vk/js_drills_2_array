const each = require("../each");

console.log(
  each([1, 2, 3, 4], (e) => {
    if (e <= 2) return e ** 2;
  })
);

console.log(each());
console.log(each([]));

console.log(
  each([1], (e) => {
    return e + 1;
  })
);
console.log(
  each([1, 2, 3], (e) => {
    console.log(e);
  })
);

console.log(
  [1, 2, 3].forEach((e) => {
    console.log(e);
  })
);
