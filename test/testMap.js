const map = require("../map");

console.log(
  map([1, 2, 3, 4], (e) => {
    return e ** 3;
  })
);
let arr = [2, 3, 4, 5];
console.log(
  map(arr, (e) => {
    return e + 10;
  })
);
console.log(arr.map((e) => e + 10));

console.log(map([]));
console.log([].map((e) => {}));
console.log(map([], () => {}));
