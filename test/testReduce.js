const reduce = require("../reduce");

console.log(reduce([1, 2, 3, 4, 5], (a, b) => a + b));
console.log([1, 2, 3, 4, 5].reduce((a, b) => a + b));
console.log(reduce([1, 2, 3, 4, 5], (a, b) => a * b, 1));
console.log([1, 2, 3, 4, 5].reduce((a, b) => a * b, 1));

console.log(reduce([], () => {}));
console.log(reduce());

console.log([1, 2, 3, 4, 5].reduce((a, b) => a + b));
