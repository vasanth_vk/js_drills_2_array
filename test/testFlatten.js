const flatten = require("../flatten");

const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

console.log(flatten(nestedArray));

console.log(nestedArray.flat(Infinity));

console.log(flatten());
console.log(flatten([]));
console.log(flatten([[[[[1]]]]]));
