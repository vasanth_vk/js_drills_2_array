// Do NOT use .reduce to complete this function.
// How reduce works: A reduce function combines all elements into a single value going from left to right.
// Elements will be passed one by one into `cb` along with the `startingValue`.
// `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
// `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
function reduce(elements, cb, startingValue) {
  if (
    elements == undefined ||
    cb == undefined ||
    typeof cb != "function" ||
    elements.length == 0
  )
    return null;

  let result = startingValue == undefined ? elements[0] : startingValue;
  for (
    let index = startingValue == undefined ? 1 : 0;
    index < elements.length;
    index++
  ) {
    result = cb(result, elements[index], index);
  }
  return result;
}

module.exports = reduce;
