// Flattens a nested array (the nesting can be to any depth).
// Hint: You can solve this using recursion.
// Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

function flatten(elements) {
  /* FLATTEN WITHOUT RECURSION*/
  //let result = [];
  //   while (elements.length != 0) {
  //     let popped = elements.pop();
  //     if (typeof popped == "object") elements.push(...popped);
  //     else result.push(popped);
  //   }
  //   return result.reverse();

  /* FLATTEN WITH RECURSION*/
  if (elements == undefined) return [];
  let result = [];
  for (let index = 0; index < elements.length; index++) {
    if (typeof elements[index] == "object") {
      result.push(...flatten(elements[index]));
    } else {
      result.push(elements[index]);
    }
  }

  return result;
}

module.exports = flatten;
